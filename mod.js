/**
 * @function isSubreddit
 * @param {string} subreddit - Subreddit name (without "r/")
 * @returns {Promise<boolean>}
 */
const isSubreddit = async subreddit => {
    const {
        status
    } = await fetch(`https://reddit.com/r/${subreddit}`, {
        method: 'HEAD'
    });
    return status === 200 || status === 403;
};

export {
    isSubreddit
}